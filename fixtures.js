const sequelize = require('./models');

console.log(`On teste la connexion à la BDD`);

sequelize.authenticate()
    .then(() => {
        console.log('Connexion établie!')

        sequelize.sync({ force: true })
            .then(() => {
                console.log('Les tables de ma BDD ont été bien générées!')
                sequelize.models.actor.create({
                    first_name: "Chris",
                    last_name: "Evans",
                    date_born: '13/06/1981'
                })
                sequelize.models.actor.create({
                    first_name: "Robert",
                    last_name: "Downey JR",
                    date_born: '04/04/1965'
                })
                sequelize.models.actor.create({
                    first_name: "Gwyneth",
                    last_name: "Paltrow",
                    date_born: '27/09/1972'
                })
                sequelize.models.actor.create({
                    first_name: "Mark",
                    last_name: "Ruffalo",
                    date_born: '22/11/1967'
                })
                sequelize.models.actor.create({
                    first_name: "Tom",
                    last_name: "Hiddleston",
                    date_born: '09/02/1981'
                })
                sequelize.models.actor.create({
                    first_name: "Chris",
                    last_name: "Hemsworth",
                    date_born: '11/08/1983'
                })
                sequelize.models.actor.create({
                    first_name: "Idris",
                    last_name: "Elba",
                    date_born: '06/09/1972'
                })
                sequelize.models.actor.create({
                    first_name: "Benedict",
                    last_name: "Cumberbatch",
                    date_born: '19/07/1976'
                })
                sequelize.models.actor.create({
                    first_name: "Benedict",
                    last_name: "Wong",
                    date_born: '03/07/1971'
                })
                sequelize.models.actor.create({
                    first_name: "Tom",
                    last_name: "Holland",
                    date_born: '01/06/1996'
                })
                sequelize.models.actor.create({
                    first_name: "Zendaya",
                    last_name: "",
                    date_born: '01/09/1996'
                })
                sequelize.models.actor.create({
                    first_name: "Scarlett",
                    last_name: "Johansson",
                    date_born: '22/11/198'
                })
                sequelize.models.movie.create({
                    title: "Avenger Endgame",
                    synopsis: "Le Titan Thanos ayant réussi à s'approprier les six Pierres d'Infinité et à les réunir sur le Gantelet doré, a pu réaliser son objectif de pulvériser la moitié de la population de l'Univers d'un claquement de doigts. Les quelques Avengers et Gardiens de la Galaxie ayant survécu, Steve Rogers, Thor, Natasha Romanoff, Tony Stark, Carol Danvers, Clint Barton, Bruce Banner, James Rhodes, Nébula et Rocket espèrent réparer le méfait de Thanos. Ils le retrouvent mais il s'avère que ce dernier a détruit les pierres et Thor le décapite. Cinq ans plus tard, alors que chacun essaie de continuer sa vie et d'oublier les nombreuses pertes dramatiques, Scott Lang, alias Ant-Man, parvient à s'échapper de la Dimension subatomique où il était coincé depuis la disparition du Docteur Hank Pym, de sa femme Janet Van Dyne et de sa fille Hope Van Dyne. Lang propose aux Avengers une solution pour faire revenir à la vie tous les êtres disparus, dont leurs alliés et coéquipiers : récupérer les Pierres d'Infinité dans le passé grâce au Royaume quantique. Pour ce faire, à l'aide des connaissances scientifiques de Bruce Banner et de Tony Stark, ils vont se scinder en plusieurs groupes pour partir chercher les gemmes dans diverses époques passées…",
                    director: "Anthony et Joe Russo",
                })
                sequelize.models.movie.create({
                    title: "Spider Man No way Home",
                    synopsis: "Pour la première fois dans son histoire cinématographique, Spider-Man, le héros sympa du quartier est démasqué et ne peut désormais plus séparer sa vie normale de ses lourdes responsabilités de super-héros. Quand il demande de l'aide à Doctor Strange, les enjeux deviennent encore plus dangereux, le forçant à découvrir ce qu'être Spider-Man signifie véritablement.",
                    director: "Jon Watts",
                })
                sequelize.models.movie_actor.create({
                    movieId: "1",
                    actorId: "1",
                })
                sequelize.models.movie_actor.create({
                    movieId: "1",
                    actorId: "2",
                })
                sequelize.models.movie_actor.create({
                    movieId: "1",
                    actorId: "3",
                })
                sequelize.models.movie_actor.create({
                    movieId: "1",
                    actorId: "4",
                })
                sequelize.models.movie_actor.create({
                    movieId: "1",
                    actorId: "5",
                })
                sequelize.models.movie_actor.create({
                    movieId: "1",
                    actorId: "6",
                })
                sequelize.models.movie_actor.create({
                    movieId: "1",
                    actorId: "7",
                })
                sequelize.models.movie_actor.create({
                    movieId: "1",
                    actorId: "9",
                })
                sequelize.models.movie_actor.create({
                    movieId: "1",
                    actorId: "10",
                })
                sequelize.models.movie_actor.create({
                    movieId: "1",
                    actorId: "11",
                })
                sequelize.models.movie_actor.create({
                    movieId: "1",
                    actorId: "12",
                })
                sequelize.models.movie_actor.create({
                    movieId: "2",
                    actorId: "8",
                })
                sequelize.models.movie_actor.create({
                    movieId: "2",
                    actorId: "9",
                })
                sequelize.models.movie_actor.create({
                    movieId: "2",
                    actorId: "10",
                })
                sequelize.models.movie_actor.create({
                    movieId: "2",
                    actorId: "11",
                })
            })
    })
    .catch((err) => {
        console.log(`Ma BDD plante, voici l'erreur: ${err}`)
    })