const { DataTypes, Sequelize } = require('sequelize');

const sequelize = new Sequelize('sqlite:database.db')

const Actor = require('./Actor')(sequelize, DataTypes);
const Movie = require('./Movie')(sequelize, DataTypes);
const Testimonial = require('./Testimonial')(sequelize, DataTypes);

Movie.belongsToMany(Actor, {through: 'movie_actor'})
Actor.belongsToMany(Movie, {through: 'movie_actor'})

Testimonial.belongsToMany(Movie, {through: 'movie_testimonial'});
Movie.belongsToMany(Testimonial, {through: 'movie_testimonial'});

module.exports = sequelize;