function Movie(sequelize, DataTypes) {
    return sequelize.define('movie', {
        title: {
            type: DataTypes.STRING(200),
            allowNull: false
        },
        synopsis: {
            type:DataTypes.TEXT
        },
        director: {
            type: DataTypes.STRING(200),
            allowNull: false
        }
    })
}

module.exports = Movie;