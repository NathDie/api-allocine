function Testimonial(sequelize, DataTypes) {
    return sequelize.define('testimonial', {
        pseudo: {
            type: DataTypes.STRING(25),
            allowNull: false
        },
        message: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        nb_stars: {
            type:DataTypes.INTEGER,
            allowNull: false
        },
    })
}

module.exports = Testimonial;