function Actor(sequelize, DataTypes) {
    return sequelize.define('actor', {
        first_name: {
            type: DataTypes.STRING(25),
            allowNull: false
        },
        last_name: {
            type: DataTypes.STRING(25),
            allowNull: false
        },
        date_born: {
            type:DataTypes.TEXT,
            allowNull: false
        },
    })
}

module.exports = Actor;