const express = require('express');
const cors = require('cors');
const sequelize = require('./models');

const app = express();
const port = 4265;

app.use(cors());

app.use(express.json());
app.use(express.urlencoded());


const moviesRouter = require('./router/moviesRouter');
app.use('/movies', moviesRouter);

const actorsRouter = require('./router/actorsRouter');
app.use('/actors', actorsRouter);

const testimonialsRouter = require('./router/testimonialsRouter');
app.use('/testimonials', testimonialsRouter);

sequelize.authenticate()
    .then(() => {
        console.log("Database connection OK!");
        app.listen(port, () => {
            console.log(`App listening at http://localhost:${port}`);
        })
    })
    .catch(err => {
        console.log('Unable to connect to the database');
        console.log(err.message);
        process.exit();
    })