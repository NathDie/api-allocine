# API ALLOCINE

An simple API made with express and sequelize

## INFORMATIONS

Port used : 4265

## STRUCTURE DB

Movies
```bash
title
synopsis
director
```

Actor
```bash
first_name
last_name
date_born
```

Testimonial
```bash
pseudo
message
nb_stars
```

## REQUEST API

Movies
```bash
get all movies : http://localhost/movies
get movie by id : http://localhost/movies/:idMovie
post movie : http://localhost/movies
patch movie by id : http://localhost/movies/:idMovie
delete movie by id : http://localhost/movies/:idMovie
```

Actors
```bash
get all actors : http://localhost/actors
get actor by id : http://localhost/actors/:idActor
post actor & assign to specific movie : http://localhost/actors/movie/:idMovie
patch actor by id : http://localhost/actors/:idActor
delete actor by id : http://localhost/actors/:idActor
```

Testimonials
```bash
get all testimonials : http://localhost/testimonials
get testimonial by id : http://localhost/testimonials/:idTestimoniel
post testimonial & assign to specific movie : http://localhost/testimonials/movie/:idMovie
delete testimonial by id : http://localhost/testimonials/:idTestimoniel
```

## License
[MIT](https://choosealicense.com/licenses/mit/)