const express = require("express");
const router = express.Router();
const sequelize = require("../models");

router.get('/', (req, res) => {
    sequelize.models.actor.findAll()
        .then(actors => res.json(actors))
})

router.get('/:actorId',(req,res) => {
    sequelize.models.actor.findByPk(req.params.actorId)
        .then(actor => res.json(actor))
})

router.post('/', (req, res ) => {
    sequelize.models.actor.create(req.body)
        .then(actorCreated => {
            res.status(201).json(actorCreated);
        })
})

router.post('/movie/:movieId', (req, res) => {
    sequelize.models.actor.create(req.body)
        .then(actorCreated => {
            sequelize.models.movie_actor.create({
                movieId: req.params.movieId,
                actorId: actorCreated.id
            })
            res.status(201).json(actorCreated);
        })
})

router.patch('/:actorId', (req, res) => {
    sequelize.models.movie.update(req.body,
        {where: {id : req.params.actorId} })
        .then(nbRowsUpdated => {
            res.json(nbRowsUpdated);
        })
})

router.delete("/:actorId", (req, res) => {
    sequelize.models.actor
        .destroy({
            where: {
                id: req.params.actorId,
            },
        })
        .then(res.json({ msg: "l'acteur a été supprimé" }));
});

module.exports =  router;