const express = require("express");
const router = express.Router();
const sequelize = require("../models");

router.get('/', (req, res) => {
    sequelize.models.testimonial.findAll()
        .then(testimonials => res.json(testimonials))
})

router.get('/:testimonialId',(req,res) => {
    sequelize.models.testimonial.findByPk(req.params.testimonialId)
        .then(testimonial => res.json(testimonial))
})

router.post('/movie/:movieId', (req, res ) => {
    if (req.body.nb_stars >= 0 && req.body.nb_stars <= 5) {
        sequelize.models.testimonial.create(req.body)
            .then(testimonialCreated => {
                sequelize.models.movie_testimonial.create({
                    movieId: req.params.movieId,
                    testimonialId: testimonialCreated.id
                })
                res.status(201).json(testimonialCreated);
            })
    } else {
        res.status(500).json({ message: "Please enter a number of stars between 0 and 5"})
    }
})

router.patch('/:testimonialId', (req, res) => {
    if (req.body.nb_stars >= 0 && req.body.nb_stars <= 5) {
        sequelize.models.testimonial.update(req.body,
            {where: {id : req.params.testimonialId} })
            .then(nbRowsUpdated => {
                res.json(nbRowsUpdated);
            })
    } else {
        res.status(500).json({ message: "Please enter a number of stars between 0 and 5"})
    }
})

router.delete("/:testimonialId", (req, res) => {
    sequelize.models.testimonial
        .destroy({
            where: {
                id: req.params.testimonialId,
            },
        })
        .then(res.json({ msg: "l'avis a été supprimé" }));
});

module.exports =  router;